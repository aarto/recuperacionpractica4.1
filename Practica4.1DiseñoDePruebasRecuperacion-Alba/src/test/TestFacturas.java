package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class TestFacturas {

	static GestorContabilidad gestor;
	Factura factura;
	
	@Before
	public void CrearFacturaAntesDeTest() {
		factura = new Factura();
		
	}

	@Test
	public void testBuscarFacturaInexistente() {
		String codigo = "1234RR";
		Factura actual = gestor.buscarFactura(codigo);
		assertFalse(gestor.listaFacturas.contains(actual));
	}
	
	@Test 
	public void testBuscarFacturaExistente() {
		Cliente nuevoCliente = new Cliente();
		Factura nuevaFactura = new Factura("45249DF", LocalDate.now(), "gafas", 9.5F, 2, nuevoCliente);
		gestor.listaFacturas.add(nuevaFactura);
		
		String codigo = "45249DF";
		Factura actual = gestor.buscarFactura(codigo);
		assertTrue(gestor.listaFacturas.contains(actual));
	}
	
	@Test
	public void testBuscarFacturaInexistenteHabiendoOtrasFacturas() {
		Cliente nuevoCliente = new Cliente();
		Cliente nuevoCliente2 = new Cliente();
		Factura nuevaFactura = new Factura("032445PL", LocalDate.now(), "zapatos", 20.3F, 3, nuevoCliente);
		Factura nuevaFactura2 = new Factura("45932M�", LocalDate.now(), "pantalones", 15.5F, 2, nuevoCliente2);
		gestor.listaFacturas.add(nuevaFactura);
		gestor.listaFacturas.add(nuevaFactura2);
		
		String codigo = "1454390VM";
		Factura actual = gestor.buscarFactura(codigo);
		assertFalse(gestor.listaFacturas.contains(actual));
	}
	
	@Test
	public void testBuscarFacturaExistenteConVariasFacturas() {
		Cliente nuevoCliente = new Cliente();
		Cliente nuevoCliente2 = new Cliente();
		Factura nuevaFactura = new Factura("10004TW", LocalDate.now(), "armario", 50F, 1, nuevoCliente);
		Factura nuevaFactura2 = new Factura("00841QF", LocalDate.now(), "lamparas", 20.3F, 3, nuevoCliente2);
		gestor.listaFacturas.add(nuevaFactura);
		gestor.listaFacturas.add(nuevaFactura2);
		
		String codigo = "10004TW";
		Factura actual = gestor.buscarFactura(codigo);
		assertTrue(gestor.listaFacturas.contains(actual));
	}

	@Test
	public void testA�adirNuevaFactura() {
		Cliente nuevoCliente = new Cliente ();
		Factura nuevaFactura = new Factura("21212DD", LocalDate.now(), "alfombra", 50F, 1, nuevoCliente);
		gestor.crearFactura(nuevaFactura);
		assertTrue(gestor.listaFacturas.contains(nuevaFactura));
		
	}
	
	@Test
	public void testA�adirNuevaFacturaConMismoCodigo() {
		Cliente nuevoCliente = new Cliente ();
		Cliente nuevoCliente2 = new Cliente ();
		Factura nuevaFactura = new Factura("303030ZX", LocalDate.now(), "libro", 10F, 3, nuevoCliente);
		Factura nuevaFactura2 = new Factura("303030ZX", LocalDate.now(), "sofa", 50F, 1, nuevoCliente2);
		gestor.crearFactura(nuevaFactura);
		gestor.crearFactura(nuevaFactura2);
		assertFalse(gestor.listaFacturas.contains(nuevaFactura2));
		
	}
	
	@Test
	public void testFacturaYDniExistentes() {
		Factura nuevaFactura = new Factura("999111CC");
		Cliente nuevoCliente = new Cliente("20257850C");
		gestor.listaFacturas.add(nuevaFactura);
		gestor.listaClientes.add(nuevoCliente);
		
		String dni = "20257850C";
		String codigo = "999111CC";
		
		gestor.asignarClienteAFactura(dni, codigo);
	
		assertEquals(nuevoCliente, nuevaFactura.getCliente());
		
	}
	
	@Test
	public void testFacturaYDniInexistentes() {
		Factura nuevaFactura = new Factura("000002LL");
		Cliente nuevoCliente = new Cliente("111000XX");
		
		String dni = "000002LL";
		String codigo = "111000XX";
		
		gestor.asignarClienteAFactura(dni, codigo);
	
		assertEquals(nuevoCliente, nuevaFactura.getCliente());
		
	}
	
	@Test
	public void testFacturaExistenteYDniInexistentes() {
		Factura nuevaFactura = new Factura("000002LL");
		Cliente nuevoCliente = new Cliente("111000XX");
		gestor.listaFacturas.add(nuevaFactura);
		
		String dni = "000002LL";
		String codigo = "111000XX";
		
		gestor.asignarClienteAFactura(dni, codigo);
	
		assertEquals(nuevoCliente, nuevaFactura.getCliente());
		
	}
	
	@Test
	public void testFacturaInexistenteYDniExistentes() {
		Factura nuevaFactura = new Factura("000002LL");
		Cliente nuevoCliente = new Cliente("111000XX");
		gestor.listaClientes.add(nuevoCliente);
		
		String dni = "000002LL";
		String codigo = "111000XX";
		
		gestor.asignarClienteAFactura(dni, codigo);
	
		assertEquals(nuevoCliente, nuevaFactura.getCliente());
		
	}
	
	@Test
	public void testFacturaMasCara() {
		Cliente nuevoCliente = new Cliente("1114477G", "Teo", LocalDate.now());
		Cliente nuevoCliente2 = new Cliente("983200J", "Hector", LocalDate.now());
		Factura nuevaFactura = new Factura("900200FX", LocalDate.now(), "cuaderno", 3.3F, 6, nuevoCliente);
		Factura nuevaFactura2 = new Factura("430212Z�", LocalDate.now(), "sofa", 50F, 1, nuevoCliente2);
		gestor.listaClientes.add(nuevoCliente);
		gestor.listaClientes.add(nuevoCliente2);
		gestor.listaFacturas.add(nuevaFactura);
		gestor.listaFacturas.add(nuevaFactura2);
	
		Factura facturaCara = gestor.facturaMasCara();
	
		assertEquals(facturaCara, nuevaFactura2);
	}
	
	@Test
	public void testFacturaAnual() {
		Factura nuevaFactura = new Factura("237457LO", LocalDate.of(2018, 04, 28), "boli", 2.2F, 3, null);
		Factura nuevaFactura2 = new Factura("6534211DF", LocalDate.of(2018, 10, 02), "goma", 0.5F, 1, null);
		Factura nuevaFactura3 = new Factura("4567124VT", LocalDate.of(2018, 07, 15), "lapiz", 3F, 1, null);
		gestor.listaFacturas.add(nuevaFactura);
		gestor.listaFacturas.add(nuevaFactura2);
		gestor.listaFacturas.add(nuevaFactura3);
	
		float esperado = 5.7F;
		float facturacionAnual = gestor.calcularFacturacionAnual(2018);
		
		assertEquals(esperado, facturacionAnual, 0.0);
	}
	
	@Test
	public void testFacturaAnualDistintaSuma() {
		Factura nuevaFactura = new Factura("237457LO", LocalDate.of(2015, 04, 28), "boli", 14.6F, 3, null);
		Factura nuevaFactura2 = new Factura("80875623PZ", LocalDate.of(2015, 10, 02), "goma", 1.5F, 1, null);
		Factura nuevaFactura3 = new Factura("0909090QW", LocalDate.of(2015, 07, 15), "lapiz", 8F, 6, null);
		gestor.listaFacturas.add(nuevaFactura);
		gestor.listaFacturas.add(nuevaFactura2);
		gestor.listaFacturas.add(nuevaFactura3);
	
		float esperado = 3.4F;
		float facturacionAnual = gestor.calcularFacturacionAnual(2015);
		
		assertEquals(esperado, facturacionAnual, 0.0);
	}
	
	@Test
	public void testFacturasPorCliente() {
		Cliente nuevoCliente = new Cliente("44120978X", "Emma", LocalDate.now());
		Factura nuevaFactura = new Factura("2000LK", LocalDate.now(), "raton", 20.99F, 2, nuevoCliente);
		Factura nuevaFactura2 = new Factura("80854PZ", LocalDate.now(), "tipex", 2.75F, 5, nuevoCliente);
		Factura nuevaFactura3 = new Factura("565453DW", LocalDate.now(), "abanico", 1.50F, 8, nuevoCliente);
		gestor.listaClientes.add(nuevoCliente);
		gestor.listaFacturas.add(nuevaFactura);
		gestor.listaFacturas.add(nuevaFactura2);
		gestor.listaFacturas.add(nuevaFactura3);
		
		int esperado = 3;
		int facturasTotales = gestor.cantidadFacturasPorCliente("44120978X");
		
		assertEquals(esperado, facturasTotales);
		
	}
	
	@Test
	public void testFacturasPorClienteIncorrectas() {
		Cliente nuevoCliente = new Cliente("6712212X", "Paula", LocalDate.now());
		Factura nuevaFactura = new Factura("939393LK", LocalDate.now(), "collar", 13.95F, 6, nuevoCliente);
		Factura nuevaFactura2 = new Factura("10011PZ", LocalDate.now(), "botella", 0.75F, 18, nuevoCliente);
		Factura nuevaFactura3 = new Factura("86033DW", LocalDate.now(), "secador", 124F, 3, nuevoCliente);
		gestor.listaClientes.add(nuevoCliente);
		gestor.listaFacturas.add(nuevaFactura);
		gestor.listaFacturas.add(nuevaFactura2);
		gestor.listaFacturas.add(nuevaFactura3);
		
		int esperado = 8;
		int facturasTotales = gestor.cantidadFacturasPorCliente("6712212X");
		
		assertEquals(esperado, facturasTotales);
		
	}
	
	@Test 
	public void testEliminarFacturaInexistente() {
		Factura nuevaFactura = new Factura("564100A�");		
		String codigo = "564100A�";
		gestor.eliminarFactura(codigo);
		assertFalse(gestor.listaFacturas.contains(nuevaFactura));
	}
	
	@Test 
	public void testEliminarFacturaExistente() {
		Factura nuevaFactura = new Factura("000111VM");
		gestor.listaFacturas.add(nuevaFactura);
		
		String codigo = "000111VM";
		gestor.eliminarFactura(codigo);
		assertTrue(gestor.listaFacturas.contains(nuevaFactura));
	}
}
