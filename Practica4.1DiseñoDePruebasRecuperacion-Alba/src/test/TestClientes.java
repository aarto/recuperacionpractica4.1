package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class TestClientes {

	static GestorContabilidad gestor;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		gestor = new GestorContabilidad();
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testBuscarClienteInexistente() {
		String dni = "25205278S";
		Cliente actual = gestor.buscarCliente(dni);
		
		
		//con assertNull tambien vale
		assertFalse(gestor.listaClientes.contains(actual));
	}
	
	@Test 
	public void testBuscarClienteExistente() {
		Cliente nuevoCliente = new Cliente("14563298G", "Sergio", LocalDate.now());
		gestor.listaClientes.add(nuevoCliente);
		
		String dni = "14563298G";
		Cliente actual = gestor.buscarCliente(dni);
		assertTrue(gestor.listaClientes.contains(actual));
	}
	
	@Test
	public void testBuscarClienteInexistenteHabiendoClientes() {
		Cliente nuevoCliente = new Cliente("20990614H", "Mario", LocalDate.now());
		Cliente nuevoCliente2 = new Cliente("34526510C", "Silvia", LocalDate.now());
		gestor.listaClientes.add(nuevoCliente);
		gestor.listaClientes.add(nuevoCliente2);
		
		String dni = "14543300M";
		Cliente actual = gestor.buscarCliente(dni);
		assertFalse(gestor.listaClientes.contains(actual));
	}
	
	@Test
	public void testBuscarClienteExistenteConVariosClientes() {
		Cliente nuevoCliente = new Cliente("11223344W", "Kyle", LocalDate.now());
		Cliente nuevoCliente2 = new Cliente("00886644F", "Roberto", LocalDate.now());
		gestor.listaClientes.add(nuevoCliente);
		gestor.listaClientes.add(nuevoCliente2);
		
		String dni = "11223344W";
		Cliente actual = gestor.buscarCliente(dni);
		assertTrue(gestor.listaClientes.contains(actual));
	}
	
	@Test
	public void testAnnaadirNuevoCliente() {
		Cliente nuevoCliente = new Cliente ("00000000A");
		gestor.altaCliente(nuevoCliente);
		assertTrue(gestor.listaClientes.contains(nuevoCliente));
		
	}
	
	@Test
	public void testAnnaadirNuevoClienteConMismoDNI() {
		Cliente nuevoCliente = new Cliente ("91287346P");
		Cliente nuevoCliente2 = new Cliente ("91287346P");
		gestor.altaCliente(nuevoCliente);
		gestor.altaCliente(nuevoCliente2);
		assertFalse(gestor.listaClientes.contains(nuevoCliente2));
		
	}

	@Test
	public void testClienteMasAntiguo() {
		Cliente clienteNuevo = new Cliente("36721076J", "Ana", LocalDate.parse("2017-05-24"));
		Cliente clienteNuevo2 = new Cliente("36721076J", "Ana", LocalDate.parse("2018-03-24"));
		gestor.listaClientes.add(clienteNuevo);
		gestor.listaClientes.add(clienteNuevo2);
	
		Cliente clienteAntiguo = gestor.clienteMasAntiguo();
		
		assertEquals(clienteAntiguo, clienteNuevo2);
		
	}

	@Test 
	public void testEliminarClienteSinFacturaAsignada() {
		Cliente nuevoCliente = new Cliente("23435414V", "Mark", LocalDate.now());
		gestor.listaClientes.add(nuevoCliente);
		
		String dni = "23435414V";
		gestor.eliminarCliente(dni);
		assertFalse(gestor.listaClientes.contains(nuevoCliente));
	}
	
	@Test 
	public void testEliminarClienteConFacturaAsignada() {
		Cliente nuevoCliente = new Cliente("23435414V", "Mark", LocalDate.now());
		Factura nuevaFactura = new Factura ("303022VV", LocalDate.now(), "silla", 50F, 1, nuevoCliente);
		gestor.listaClientes.add(nuevoCliente);
		gestor.listaFacturas.add(nuevaFactura);
		
		String dni = "23435414V";
		gestor.eliminarCliente(dni);
		assertTrue(gestor.listaClientes.contains(nuevoCliente));
	}
}
